const dbUtils = require('./utils/dbUtils');

async function hello(event, context) {
  let rows = dbUtils.runSelectQuery("select name from OrganizationalUnit where Id=2;");

  return {
    statusCode: 200,
    body: JSON.stringify({ message: (rows[1].name) }),
  };
}

export const handler = hello;


