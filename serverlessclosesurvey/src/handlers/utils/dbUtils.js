import { mysql } from 'mysql';

// Use this code snippet in your app.
// If you need more information about configurations or implementing the sample code, visit the AWS docs:
// https://aws.amazon.com/developers/getting-started/nodejs/

// Load the AWS SDK
import { SecretsManager } from 'aws-sdk';

var region = "ca-central-1";
var secretName = "closeSurveyCredentials";
var secret;
var decodedBinarySecret;

// Create a Secrets Manager client
var client = new SecretsManager({
    region: region
});

// In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
// See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
// We rethrow the exception by default.

client.getSecretValue({SecretId: secretName}, function(err, data) {
    if (err) {
        if (err.code === 'DecryptionFailureException')
            // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw err;
        else if (err.code === 'InternalServiceErrorException')
            // An error occurred on the server side.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw err;
        else if (err.code === 'InvalidParameterException')
            // You provided an invalid value for a parameter.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw err;
        else if (err.code === 'InvalidRequestException')
            // You provided a parameter value that is not valid for the current state of the resource.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw err;
        else if (err.code === 'ResourceNotFoundException')
            // We can't find the resource that you asked for.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw err;
    }
    else {
        // Decrypts secret using the associated KMS CMK.
        // Depending on whether the secret is a string or binary, one of these fields will be populated.
        if ('SecretString' in data) {
            secret = data.SecretString;
        } else {
            let buff = new Buffer(data.SecretBinary, 'base64');
            decodedBinarySecret = buff.toString('ascii');
        }
    }
    
    // Your code goes here. 
});

const secretJSON = JSON.parse(secret);

const dbConfig = {
    host: secretJSON.host,
    user: secretJSON.username,
    password: secretJSON.password,
    database: "tlb",
    port: "3306",
    connectionLimit: 10000,
    debug: false
}

module.exports.getConnPool = mysql.createPool(dbConfigSync());

module.exports.runSelectQuery = (sql) => {
    let connPool = mysql.createPool(dbConfigSync());

    let promise = new Promise(function (resolve, reject) {
        return connPool.query(sql, function (err, rows) {
            if (err) {//Error due to eg: query or network connection
                console.log('In reject block of runSelectQuery() for sql = ' + sql);
                //console.log('In reject block of runSelectQuery() for error = ' + err);
                reject(httpConstants.HTTP_STATUS_CODE_500);
            } else if (rows.length > 0) {//data exists
                resolve(rows);
            } else if (rows.length === 0) {//on return of no rows from the table
                //console.log('Zero rows in runSelectQuery()');
                console.log('Zero rows in runSelectQuery() = ' + sql);
                resolve(httpConstants.HTTP_STATUS_CODE_400);
            }
        });
    }).catch(function (err) {
        console.log('Error in runSelectQuery for sql = ' + sql + ' error = ' + err);
        //console.log('Error in runSelectQuery for error = ' + err);
    });

    return promise;
}